package com.company.module.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.company.module.commons.APIResponse;
import com.company.module.model.Company;
import com.company.module.service.CompanyService;

@RefreshScope
@RestController
public class CompanyController {

	@Autowired
	private CompanyService companyService;

	@Value("${company.instance.name}")
	private String instancename;

	@GetMapping(value = "/get-all-company")
	public ResponseEntity<APIResponse> getAllCompany() {
		List<Company> company = companyService.getAllCompany();
		return ResponseEntity.status(HttpStatus.OK)
				.body(new APIResponse(HttpStatus.OK.value(), true, "Company data found", company));
	}

	@GetMapping(value = "/get-all-user-company")
	public ResponseEntity<APIResponse> getAllUserCompany(@RequestParam Integer userId) {
		List<Company> company = companyService.getAllUserCompany(userId);
		return ResponseEntity.status(HttpStatus.OK)
				.body(new APIResponse(HttpStatus.OK.value(), true, "Company data found", company));
	}

	@GetMapping(value = "/check-instance")
	public ResponseEntity<APIResponse> checkInstance() {
		return ResponseEntity.status(HttpStatus.OK)
				.body(new APIResponse(HttpStatus.OK.value(), true, "User data found", instancename));
	}
}
