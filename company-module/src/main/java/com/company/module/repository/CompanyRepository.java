package com.company.module.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.company.module.model.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer> {

	List<Company> findByUserId(Integer userId);

}
