package com.company.module.service;

import java.util.List;

import com.company.module.model.Company;

public interface CompanyService {

	Company saveCompany(Company company);

	List<Company> getAllUserCompany(Integer userId);

	List<Company> getAllCompany();
}
