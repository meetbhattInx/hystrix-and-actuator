package com.company.module.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.company.module.model.Company;
import com.company.module.repository.CompanyRepository;
import com.company.module.service.CompanyService;

@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyRepository companyRepository;

	@Override
	public Company saveCompany(Company company) {
		return companyRepository.save(company);
	}

	@Override
	public List<Company> getAllUserCompany(Integer userId) {
		return companyRepository.findByUserId(userId);
	}

	@Override
	public List<Company> getAllCompany() {
		return companyRepository.findAll();
	}

}
