package com.company.module;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableEurekaClient
@EnableWebMvc
@EnableJpaRepositories
@EnableFeignClients
@EnableScheduling
@SpringBootApplication
public class CompanyModuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompanyModuleApplication.class, args);
	}

}
