package com.user.module.proxies;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.user.module.commons.APIResponse;
import com.user.module.fallbacks.CompanyFallback;

@FeignClient(name = "company-module", fallback = CompanyFallback.class)
public interface CompanyProxy {

	@GetMapping(value = "/get-all-user-company")
	public ResponseEntity<APIResponse> getUserCompany(@RequestParam Integer userId);
}
