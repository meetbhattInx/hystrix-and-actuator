package com.user.module.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.user.module.commons.APIResponse;
import com.user.module.model.UserProfile;
import com.user.module.proxies.CompanyProxy;
import com.user.module.service.UserProfileService;

@RestController
public class UserProfileController {

	private static Logger log = LoggerFactory.getLogger(UserProfileController.class.getName());

	@Autowired
	private UserProfileService userProfileService;

	@Autowired
	private CompanyProxy companyProxy;

	@Autowired
	private Environment environment;

	@GetMapping("/get-all-users")
	public ResponseEntity<APIResponse> getAllUsers() {
		List<UserProfile> userProfiles = userProfileService.getAllProfiles();
		if (null != userProfiles && !userProfiles.isEmpty()) {
			userProfiles.stream().forEach(user -> {
				ResponseEntity<APIResponse> companyResponse = companyProxy.getUserCompany(user.getUserProfileId());
				if (null != companyResponse) {
					APIResponse companyBody = companyResponse.getBody();
					if (null != companyBody) {
						user.setCompany((List<Map<String, Object>>) companyBody.getData());
					}
				} else {
					log.error("Company not found");
				}
			});
		}

		return ResponseEntity.status(HttpStatus.OK)
				.body(new APIResponse(HttpStatus.OK.value(), true, "User data found", userProfiles));
	}

	@GetMapping(value = "/check-instance")
	public ResponseEntity<APIResponse> checkInstance() {
		return ResponseEntity.status(HttpStatus.OK).body(new APIResponse(HttpStatus.OK.value(), true, "User data found",
				environment.getProperty("user.instance.name")));
	}

}
