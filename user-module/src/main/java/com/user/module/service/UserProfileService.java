package com.user.module.service;

import java.util.List;

import com.user.module.model.UserProfile;

public interface UserProfileService {

	UserProfile saveProfile(UserProfile userProfile);

	List<UserProfile> getAllProfiles();
}
