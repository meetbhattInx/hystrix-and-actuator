package com.user.module.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.user.module.model.UserProfile;
import com.user.module.repository.UserProfileRepository;
import com.user.module.service.UserProfileService;

@Service
public class UserProfileServiceImpl implements UserProfileService {

	@Autowired
	private UserProfileRepository userProfileRepository;

	@Override
	public UserProfile saveProfile(UserProfile userProfile) {
		return userProfileRepository.save(userProfile);
	}

	@Override
	public List<UserProfile> getAllProfiles() {
		return userProfileRepository.findAll();
	}

}
