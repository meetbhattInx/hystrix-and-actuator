package com.user.module.fallbacks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.user.module.commons.APIResponse;
import com.user.module.proxies.CompanyProxy;

@Component
public class CompanyFallback implements CompanyProxy {

	private static Logger log = LoggerFactory.getLogger(CompanyFallback.class.getName());

	@Override
	public ResponseEntity<APIResponse> getUserCompany(Integer userId) {
		log.info("fallback for company called");
		return null;
	}

}
